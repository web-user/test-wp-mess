<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'dev_theme_red' );

/** MySQL database username */
define( 'DB_USER', 'pem' );

/** MySQL database password */
define( 'DB_PASSWORD', 'qwerty' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',          '^3m(0?hTt-X6)vE-D^Dg50_Ers,c$BH0)fdKn2lz!RrX.Y~}pgnTc%2y5bbZpk?`' );
define( 'SECURE_AUTH_KEY',   '-;:Lv(SIkt2F.6YVoFdX?7X&V)O5M+=doa2X?E[{hqi[)p9>JAwKJ6*(H~o9AJ|^' );
define( 'LOGGED_IN_KEY',     'Im)4sdCo4f1`ax*~j<a;.y} {!$%0G-|;ki2EY_ydH=04[-=d%b LsrVWS-Y9M3;' );
define( 'NONCE_KEY',         '@aQh&@^lXc/I^~dLMvD]:]Db,>~~=#E>f7JFzPEY&:LL2Zi2neOD~:|fJXSKyiJd' );
define( 'AUTH_SALT',         '7lc)W!15G:(pH=+-;ltIXw/?V`/#$#>}-OD8SbTBiJ+SQtdHOeFl#mA_l^:=:SLa' );
define( 'SECURE_AUTH_SALT',  '|MS:Dp^l3m$m,CM;Zw-5+,->`7Ye94EmYu%~Gmr;(?`pK#c6nswB(NQfWlD|>#3j' );
define( 'LOGGED_IN_SALT',    '&*UNIeL)TR}LFoe1Z^zX0>f.!E*-<?A9[e@)-a2/ uI*;![8o*e#wSbhlyQvPV^K' );
define( 'NONCE_SALT',        '6P*U[/_~P7tf9&`rc@v0RQf[i]]Z*kk,P=o*eyTs!K{wU`%]$V4}f*Q!yY/sI~Xp' );
define( 'WP_CACHE_KEY_SALT', '5o>g7TwlVXh^wlM9k{{f2hoOd-}Z;9`WblA}vR/5QV>2$)t[jDFwK/([4TE} zJC' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */

define( 'WP_DEBUG', true );

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
