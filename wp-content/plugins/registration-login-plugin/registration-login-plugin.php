<?php
/*
  Plugin Name: Custom Registration and login
  Plugin URI:
  Description: Custom Registration and login.
  Version: 1.0
  Author: Test
  Author URI:
 */




function wordpress_custom_registration_form( $first_name, $username, $password, $email) {
    global $username, $password, $email, $first_name, $last_name;
    echo '
    <form action="' . $_SERVER['REQUEST_URI'] . '" method="post">
   First Name :
    <input type="text" name="fname" value="' . ( isset( $_POST['fname']) ? $first_name : null ) . '">
    Username <strong>*</strong>
    <input type="text" name="username" value="' . ( isset( $_POST['username'] ) ? $username : null ) . '">
    Password <strong>*</strong>
    <input type="password" name="password" value="' . ( isset( $_POST['password'] ) ? $password : null ) . '">
    Email: <strong>*</strong>
    <input type="text" name="email" value="' . ( isset( $_POST['email']) ? $email : null ) . '">
   <input type="submit" name="submit" value="Register"/>
    </form>
    ';

}


/* ****************************************************************************** */
function auto_login_new_user_after_registration( $user_id ) {


    if ( isset($_POST['mess_dad']) && $_POST['mess_dad'] == "yes" ) {
 // Need to verify that new user registration came from this plugin

            wp_set_current_user($user_id);
            wp_set_auth_cookie($user_id);
            wp_redirect( home_url() );


        }
}
add_action( 'user_register', 'auto_login_new_user_after_registration' );






//[wp_registration_form]
add_shortcode( 'wp_registration_form', 'wp_custom_shortcode_registration' );

function wp_custom_shortcode_registration() {
   ?>

    <form name="registerform" id="registerform" action="<?php echo esc_url( site_url( 'wp-login.php?action=register', 'login_post' ) ); ?>" method="post" novalidate="novalidate">
        <p>
            <label for="user_login"><?php _e('Username') ?><br />
                <input type="text" name="user_login" id="user_login" class="input" value="" size="20" /></label>
        </p>
        <p>
            <label for="user_email"><?php _e('Email') ?><br />
                <input type="email" name="user_email" id="user_email" class="input" value="" size="25" /></label>
        </p>
        <?php
        /**
         * Fires following the 'Email' field in the user registration form.
         *
         * @since 2.1.0
         */
        do_action( 'register_form' );

        $password1 = ( ! empty( $_POST['password1'] ) ) ? trim( $_POST['password1'] ) : '';
        $password2 = ( ! empty( $_POST['password2'] ) ) ? trim( $_POST['password2'] ) : '';
        ?>
        <p>
            <label for="password1"><?php _e( 'Password:','auto-login-new-user-after-registration') ?>
                <input type="password" name="password1" id="password1" class="input" value="<?php echo esc_attr( wp_unslash( $password1 ) ); ?>" size="25" /></label><br>
            <label for="password2"><?php _e( 'Confirm Password:','auto-login-new-user-after-registration') ?>
                <input type="password" name="password2" id="password2" class="input" value="<?php echo esc_attr( wp_unslash( $password2 ) ); ?>" size="25" /></label><br>
        </p>
        <p id="reg_passmail"><?php _e( 'Registration confirmation will be emailed to you.' ); ?></p>

        <input type="hidden" name="mess_dad" id="mess_dad" value="yes">
        <br class="clear" />
        <input type="hidden" name="redirect_to" value="<?php echo esc_attr( home_url() ); ?>" />
        <p class="submit"><input type="submit" name="wp-submit" id="wp-submit" class="button button-primary b
        utton-large" value="<?php esc_attr_e('Register'); ?>" /></p>
    </form>
<?php }



// user login form
//[login_form]
function custom_login_form() {

    if(!is_user_logged_in()) {

        global $pippin_load_css;

        // set this to true so the CSS is loaded
        $pippin_load_css = true;

        $output = '<form name="loginform" id="loginform" action="' . site_url( '/wp-login.php/' ) . '" method="post">
<p>Username: <input id="user_login" type="text" size="20" required value="" name="log"></p>
<p>Password: <input id="user_pass" type="password" size="20" required value="" name="pwd"></p>
<p><input id="rememberme" type="checkbox" value="forever" name="rememberme"></p>

<p><input id="wp-submit" type="submit" value="Login" name="wp-submit"></p>

<input type="hidden" value="/" name="redirect_to">
<input type="hidden" value="1" name="testcookie">
</form>';
    } else {
        // could show some logged in user info here
         $output = 'user info here';
    }
    return $output;
}
add_shortcode('login_form', 'custom_login_form');

/**
 * Register a custom post type called "message".
 *
 * @see get_post_type_labels() for label keys.
 */
function wp_message_custom_init() {
    $labels = array(
        'name'                  => _x( 'Messages', 'Post type general name', 'textdomain' ),
        'singular_name'         => _x( 'Message', 'Post type singular name', 'textdomain' ),
        'menu_name'             => _x( 'Messages', 'Admin Menu text', 'textdomain' ),
        'name_admin_bar'        => _x( 'Message', 'Add New on Toolbar', 'textdomain' ),
        'add_new'               => __( 'Add New', 'textdomain' ),
        'add_new_item'          => __( 'Add New Message', 'textdomain' ),
        'new_item'              => __( 'New Message', 'textdomain' ),
        'edit_item'             => __( 'Edit Message', 'textdomain' ),
        'view_item'             => __( 'View Message', 'textdomain' ),
        'all_items'             => __( 'All Messages', 'textdomain' ),
        'search_items'          => __( 'Search Messages', 'textdomain' ),
        'parent_item_colon'     => __( 'Parent Messages:', 'textdomain' ),
        'not_found'             => __( 'No Messages found.', 'textdomain' ),
        'not_found_in_trash'    => __( 'No Messages found in Trash.', 'textdomain' ),
        'featured_image'        => _x( 'Message Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'archives'              => _x( 'archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'textdomain' ),
        'insert_into_item'      => _x( 'Insert into message', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'textdomain' ),
        'uploaded_to_this_item' => _x( 'Uploaded to this', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'textdomain' ),
        'filter_items_list'     => _x( 'Filter list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'textdomain' ),
        'items_list_navigation' => _x( 'Messages list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'textdomain' ),
        'items_list'            => _x( 'Messages list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'textdomain' ),
    );

    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'message' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'author',  'comments' ),

    );

    register_post_type( 'message', $args );
}

add_action( 'init', 'wp_message_custom_init' );