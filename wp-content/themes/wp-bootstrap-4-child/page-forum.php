<?php
/**
 * Template Name: Forum page Custom
 *
 * @package WordPress
 */

get_header() ?>

<?php
$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
$args = array(
        'post_type' => 'message',
    'posts_per_page' => 3,
    'paged' => $paged
);
$the_query = new WP_Query( $args );
?>
<div class="mes-page-cust">

    <div class="container">
        <div class="row">
            <div class="col-md-12 wp-bp-content-width">
                <?php if ( is_user_logged_in() ) : ?>

                <?php if ( $the_query->have_posts() ) : $count = 0; ?>
                <div class="chat">

                    <div class="chat-history">
                        <ul>
                            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                <?php if( $count % 2 == 0 ): ?>
                                    <li class="clearfix" id="post-<?php the_ID(); ?>">
                                        <div class="message-data align-right">
                                            <?php wp_bootstrap_4_posted_on(); ?>
                                        </div>

                                        <div class="message other-message float-right">
                                            <header class="entry-header">
                                                <?php the_title( '<h1 class="entry-title h2">', '</h1>' ); ?>
                                            </header><!-- .entry-header -->
                                            <?php the_content(); ?>
                                        </div>
                                        <div class="">
                                            <button type="button" class="btn btn-info com-send-mes float-right" data-id-com="<?php echo get_the_ID(); ?>"><?php _e( 'Comments' ); ?></button>
                                        </div>

                                        <div class="block-com-mes" id="bl-com-<?php echo get_the_ID(); ?>" style="display: none">
                                            <?php
                                            // If comments are open or we have at least one comment, load up the comment template.
                                            if ( comments_open() || get_comments_number() ) :
                                                comments_template();
                                            endif; ?>
                                        </div>
                                    </li>

                                <?php else:  ?>
                                    <li id="post-<?php the_ID(); ?>">
                                        <div class="message-data">
                                            <?php wp_bootstrap_4_posted_on(); ?>
                                        </div>

                                        <div class="message my-message">
                                            <header class="entry-header">
                                                <?php the_title( '<h1 class="entry-title h2">', '</h1>' ); ?>
                                            </header><!-- .entry-header -->
                                            <?php the_content(); ?>
                                        </div>
                                        <div class="cont-btn-comm">
                                            <button type="button" class="btn btn-info com-send-mes float-right" data-id-com="<?php echo get_the_ID(); ?>"><?php _e( 'Comments' ); ?></button>
                                        </div>
                                        <div class="block-com-mes" id="bl-com-<?php echo get_the_ID(); ?>" style="display: none">
                                            <?php
                                            // If comments are open or we have at least one comment, load up the comment template.
                                            if ( comments_open() || get_comments_number() ) :
                                                comments_template();
                                            endif; ?>
                                        </div>
                                    </li>
                                <?php endif; ?>

                            <?php $count ++; endwhile;
                            wp_reset_postdata();
                            ?>
                        </ul>
                        <nav>
                            <!-- Pagination -->
                            <?php $pagenavi = mess_pagenavi_array($the_query); ?>
                            <?php if (is_array($pagenavi)) : ?>
                                <ul class="pagination justify-content-center mess__items_pagination">
                                    <?php foreach ($pagenavi as $link) : ?>
                                        <?php
                                        preg_match('/<span class="(.*?) dots">(.*?)<\/span>/uis', $link, $matches);
                                        if (count($matches) > 0) {
                                            $link = '<div class="page-gap"><div></div><div></div><div></div></div>';
                                        }
                                        ?>
                                        <li class="page-item"><?php echo $link; ?></li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                            <!-- /Pagination -->
                        </nav>
                    </div>
                </div>
                <?php else:  ?>
                    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                <?php endif; ?>
                <div class="container">
                    <div class="row justify-content-start">
                            <article class="card mt-3r">
                                <div class="card-body">
                                    <!-- New Post Form -->
                                    <div id="postbox" >
                                        <form id="new_post" name="new_post" method="post" action="" lass="needs-validation" >

                                            <!-- post name -->
                                            <p><label for="title">Title</label><br />
                                                <input type="text" id="title" value="" class="form-control" required tabindex="1" size="20" name="title" />

                                            </p>

                                            <!-- post Content -->
                                            <p><label for="description">Content</label><br />
                                                <textarea id="description" tabindex="3" name="description" cols="50" class="form-control" required="true" rows="6"></textarea>
                                            </p>
                                            <p align="right"><input type="submit" value="Publish" tabindex="6" id="submit" name="submit" /></p>

                                            <input type="hidden" name="action" value="new_post" />
                                            <input type="hidden" name="mess" value="add">
                                            <?php wp_nonce_field( 'new-post' ); ?>

                                        </form>
                                    </div>
                                </div>
                            </article>

                    </div>
                </div>
            </div>
            <?php else: ?>
                <article class="card mt-3r">
                    <div class="card-body">
                        <p><?php _e( 'Sorry, need login.' ); ?></p>
                    </div>
                </article>

            <?php endif; ?>
        </div>
    </div>
</div>
<?php get_footer() ?>
