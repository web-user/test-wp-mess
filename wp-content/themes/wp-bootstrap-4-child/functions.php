<?php

add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles()
{
    $parenthandle = 'parent-style'; // This is  for the theme.
    $theme = wp_get_theme();
    wp_enqueue_style($parenthandle, get_template_directory_uri() . '/style.css',
        array(),  // if the parent theme code has a dependency, copy it to here
        $theme->parent()->get('Version')
    );
    wp_enqueue_style('child-style', get_stylesheet_uri(),
        array($parenthandle),
        $theme->get('Version') // this only works if you have Version in the style header
    );

    wp_enqueue_style( 'my-style-page', get_stylesheet_directory_uri() . '/css/custom-page.css', false, '1.0', 'all' );

    wp_enqueue_script( 'custom-mes-js', get_stylesheet_directory_uri() . '/js/custom-mes.js', array(), $theme->get('Version'), true );
    wp_script_add_data( 'custom-mes-js', 'async', true );

}

function wpse_58613_comment_redirect( $location ) {
    return '/forum';
}
add_filter( 'comment_post_redirect', 'wpse_58613_comment_redirect' );


function your_function() {
    ?>
    <script>
        // $("#test-mes").slideDown("slow");
    </script>
<?php
}
add_action( 'wp_footer', 'your_function' );


function send_mes_page(){
    if( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) &&  $_POST['action'] == "new_post" &&  $_POST['mess'] == "add") {

        // Do some minor form validation to make sure there is content
        if (isset ($_POST['title'])) {
            $title =  $_POST['title'];
        } else {
            exit();
        }
        if (isset ($_POST['description'])) {
            $description = $_POST['description'];
        } else {
            echo 'Please enter the content';
        }

        // Add the content of the form to $post as an array
//        $page['comment_status'] = 'closed'; // allowed values: 'closed' or 'open'
        $new_post = array(
            'post_title'    => $title,
            'post_content'  => $description,

            'post_status'   => 'publish',           // Choose: publish, preview, future, draft, etc.
            'post_type' => 'message',  //'post',page' or use a custom post type if you want to
            'comment_status' => 'open'// allowed values: 'closed' or 'open'
        );
        //save the new post
        $pid = wp_insert_post($new_post);
        //insert taxonomies
        wp_redirect( '/forum' ); exit;
    }
}

send_mes_page();


/**
 * Pagination
 *
 * @since Evromisto 1.0
 */


function mess_pagenavi_array($wp_query) {
    $big = 999999999;

    $args = array(
        'base'      => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
        'type'      => 'array',
        'mid_size'  => 1,
        'end_size'  => 1,
        'prev_text' => '<i class="fa fa-angle-left" aria-hidden="true"></i><span class="sr-only">Previous</span>',
        'next_text' => '<i class="fa fa-angle-right" aria-hidden="true"></i><span class="sr-only">Next</span>',
        'current'   => max( 1, get_query_var('paged') ),
        'total'     => $wp_query->max_num_pages,
    );

    $result = paginate_links( $args );

    $result = str_replace( '/page/1/', '', $result );

    return $result;
}


